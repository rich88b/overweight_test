FactoryGirl.define do
  factory :tracking do
    tracking_number {Faker::Number.number(digits: 15).to_s}
    carrier "FEDEX"
    label_package FactoryGirl.create(:package)
    real_package FactoryGirl.create(:package)
  end
end
