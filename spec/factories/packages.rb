FactoryGirl.define do
  factory :package do |f|
    f.length {Faker::Number.decimal(l_digits: 2, r_digits: 2)}
    f.width {Faker::Number.decimal(l_digits: 2, r_digits: 2)}
    f.height {Faker::Number.decimal(l_digits: 2, r_digits: 2)}
    f.weight {Faker::Number.decimal(l_digits: 2, r_digits: 2)}
    f.distance_unit 'IN'
    f.mass_unit 'LB'
  end

end
