require 'rails_helper'

RSpec.describe Package, type: :model do

  context 'validation tests' do
    it "has a valid factory" do
      expect(FactoryGirl.create(:package)).to be_valid
    end
    it "is invalid without a length" do
      expect(FactoryGirl.build(:package, length: nil)).to_not be_valid
    end
    it "is invalid without a width" do
      expect(FactoryGirl.build(:package, width: nil)).to_not be_valid
    end
    it "is invalid without a height" do
      expect(FactoryGirl.build(:package, height: nil)).to_not be_valid
    end
    it "is invalid without a weight" do
      expect(FactoryGirl.build(:package, weight: nil)).to_not be_valid
    end
    it "is invalid without a distance_unit" do
      expect(FactoryGirl.build(:package, distance_unit: nil)).to_not be_valid
    end
    it "is invalid without a mass_unit" do
      expect(FactoryGirl.build(:package, mass_unit: nil)).to_not be_valid
    end
  end

  context 'functions' do
    it 'return volumetric weight' do
      package = FactoryGirl.create(:package)
      expect(package.get_volumetric_weight).to_not be_nil
    end

    it 'return total weight' do
      package = FactoryGirl.create(:package)
      expect(package.get_total_weight).to_not be_nil
    end
    it 'return weight label' do
      package = FactoryGirl.create(:package)
      package_decorator = ApplicationController.helpers.decorate(package)
      expect(package_decorator.weight_label).to_not be_nil
    end

    it 'convert metric system from lb to kg' do
      package = FactoryGirl.build(:package, weight: 1, mass_unit: 'LB', )
      expect(package.get_real_weight).to eq 0.453592
    end

    it 'convert metric system from in to cm' do
      package = FactoryGirl.build(:package, length: 50, width: 50, height: 50, distance_unit: 'IN')
      puts package.get_volumetric_weight
      expect(package.get_volumetric_weight).to eq 409.6766
    end
  end


end
