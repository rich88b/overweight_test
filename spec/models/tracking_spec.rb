require 'rails_helper'

RSpec.describe Tracking, type: :model do
  context 'validation tests' do
    it "has a valid factory" do
      expect(FactoryGirl.create(:tracking)).to be_valid
    end
    it "is invalid without a tracking number" do
      expect(FactoryGirl.build(:tracking, tracking_number: nil)).to_not be_valid
    end

    it "is invalid without a carrier" do
      expect(FactoryGirl.build(:tracking, carrier: nil)).to_not be_valid
    end
    it "is invalid without a label package data (from file)" do
      expect(FactoryGirl.build(:tracking, label_package: nil)).to_not be_valid
    end
    it "is invalid without a real package data (from carrier)" do
      expect(FactoryGirl.build(:tracking, real_package: nil)).to_not be_valid
    end

    it 'return overweight' do
      package = FactoryGirl.create(:package)
      package_decorator = ApplicationController.helpers.decorate(package)
      expect(package_decorator.overweight_label).to_not be_nil
    end
  end

  context 'when tracking number is unique' do
    before { FactoryGirl.build(:tracking, tracking_number: '13') }
    it { expect(FactoryGirl.build(:tracking, tracking_number: '123')).to be_valid}
  end

end
