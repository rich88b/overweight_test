require "rails_helper"

RSpec.describe TrackingsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/trackings").to route_to("trackings#index")
    end

    it "routes to #load_labels_file" do
      expect(:post => "/load_labels_file").to route_to("trackings#load_labels_file")
    end

  end
end
