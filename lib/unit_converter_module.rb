module UnitConverterModule

  class UnitConverter
    attr_accessor :converter, :unit_from

    def initialize(converter, unit_from)
      @converter = converter
      @unit_from = unit_from
    end

    def converter_unit_to_universal(quantity)
      @converter.converter_unit_to_universal(self, quantity)
    end

    def to_s(quantity)
      @converter.to_s(self, quantity)
    end
  end

  class KilogramConverter
    def converter_unit_to_universal(context, quantity)
      case context.unit_from
      when 'lb'
        quantity * 0.453592 # From lb to kg
      when 'kg'
        quantity
      else
        nil
      end
    end

    def to_s(context, quantity)
      quantity.to_s + context.unit_from
    end
  end

  class CentimeterConverter
    def converter_unit_to_universal(context, quantity)
      case context.unit_from
      when 'in'
        quantity * 2.54 # From In to cm
      when 'cm'
        quantity
      else
        nil
      end
    end

    def to_s(context, quantity)
      quantity.to_s + context.unit_from
    end
  end

end
