Rails.application.routes.draw do
  resources :packages, only:[:index]
  resources :trackings, only:[:index, :load_labels_file] do
    collection do
      post  'load_labels_file', to: 'trackings#load_labels_file'
    end
  end

  root to: 'trackings#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
