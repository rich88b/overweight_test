require_relative 'boot'

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "action_cable/engine"
require "sprockets/railtie"
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module OverweightTest
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')] # multiple locale files
    config.autoload_paths += %W(#{config.root}/lib)
    # Whitelist locales available for the application
    config.i18n.available_locales = [:en, :es]
    # Set default locale to something other than :es
    config.i18n.default_locale = :es
    config.assets.compile = true
    config.assets.precompile += %w(*.png *.jpg *.jpeg *.gif *.js *.css *.css.erb *.scss)
  end
end
