class CreateTrackings < ActiveRecord::Migration[5.0]
  def change
    create_table :trackings do |t|
      t.string :tracking_number
      t.string :carrier
      t.references :label_package, references: :package, foreign_key: { to_table: 'packages' }
      t.references :real_package, references: :package, foreign_key: { to_table: 'packages' }

      t.timestamps
    end
  end
end
