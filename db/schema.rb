# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20191229153130) do

  create_table "packages", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.float    "length",        limit: 24
    t.float    "width",         limit: 24
    t.float    "height",        limit: 24
    t.float    "weight",        limit: 24
    t.string   "distance_unit"
    t.string   "mass_unit"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "trackings", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8" do |t|
    t.string   "tracking_number"
    t.string   "carrier"
    t.integer  "label_package_id"
    t.integer  "real_package_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["label_package_id"], name: "index_trackings_on_label_package_id", using: :btree
    t.index ["real_package_id"], name: "index_trackings_on_real_package_id", using: :btree
  end

  add_foreign_key "trackings", "packages", column: "label_package_id"
  add_foreign_key "trackings", "packages", column: "real_package_id"
end
