class TrackingsDatatable
  include DatatableModule
  include UnitConverterModule
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view)
    @view = view
  end

  private
  def data
    converter = UnitConverter.new(KilogramConverter.new(), 'kg')

    fetch_data.map do |fd|
      {
          "id" => fd.id,
          "tracking_number" => fd.tracking_number,
          "carrier" => fd.carrier,
          "weight" => converter.to_s(fd.label_package.get_total_weight.ceil) +fd.label_package.get_weight_selected,
          "real_weight" => converter.to_s(fd.real_package.get_real_weight),
          "real_total_weight" => converter.to_s(fd.real_package.get_total_weight),
          "overweight_lbl" => converter.to_s(fd.get_overweight.ceil),
          "overweight" => fd.get_overweight.ceil,
          "is_overweight" => fd.get_overweight.ceil == 0 ? I18n.t('ui.tracking.hasnt_overweight') : '',
      }

    end
  end

  def fetch_records
    exercises = Tracking.joins(:label_package, :real_package).includes(:label_package, :real_package).all
    exercises = filter(exercises)
    exercises
  end

end
