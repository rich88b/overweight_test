class Tracking < ApplicationRecord

  belongs_to :label_package, class_name: "Package"
  belongs_to :real_package, class_name: "Package"

  validates :tracking_number, presence: true, uniqueness: true
  validates :carrier, presence: true
  validates :label_package, presence: true
  validates :real_package, presence: true

  def get_overweight
    overweight = self.real_package.get_total_weight - self.label_package.get_total_weight.ceil
    overweight > 0 ? overweight : 0
  end

end
