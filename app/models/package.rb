class Package < ApplicationRecord

  include UnitConverterModule

  validates :length, presence: true
  validates :width, presence: true
  validates :height, presence: true
  validates :weight, presence: true
  validates :distance_unit, presence: true
  validates :mass_unit, presence: true

  has_one :label_tracking, :class_name => 'Tracking', foreign_key: 'label_package_id'
  has_one :real_tracking, :class_name => 'Tracking', foreign_key: 'real_package_id'

  def get_real_weight
    converter = UnitConverter.new(KilogramConverter.new(), self.mass_unit.downcase)
    converter.converter_unit_to_universal(self.weight)
  end

  def get_total_weight
    volumetric_weight = self.get_volumetric_weight
    converter = UnitConverter.new(KilogramConverter.new(), self.mass_unit.downcase)
    weight = converter.converter_unit_to_universal(self.weight)
    volumetric_weight > weight ? volumetric_weight : weight
  end

  def get_volumetric_weight
    converter = UnitConverter.new(CentimeterConverter.new(), self.distance_unit.downcase)
    (converter.converter_unit_to_universal(self.width) * converter.converter_unit_to_universal(self.height) * converter.converter_unit_to_universal(self.length)) / 5000
  end

  def get_weight_selected
    self.get_total_weight == self.weight ? '' : '(Volumetrico)'
  end

end
