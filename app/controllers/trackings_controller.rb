class TrackingsController < ApplicationController

  include Adapter

  def index
    respond_to do |format|
      format.html{ @tracking = Tracking.new }
      format.json {render json: TrackingsDatatable.new(view_context)}
    end
  end

  def load_labels_file

    if params[:upload][:file].present?
      carrier = nil
      file = File.read(params[:upload][:file].original_filename)
      data_labels = JSON.parse(file)

      data_labels.each do |label_data|

        if carrier.nil? && label_data['carrier'] == 'FEDEX'
          carrier = FedexAdapter.new
        end

        unless carrier.nil?
          tracking = Tracking.new(tracking_number: label_data['tracking_number'], carrier: label_data['carrier'])
          tracking.label_package = Package.new(length: label_data['parcel']['length'],
                                               width: label_data['parcel']['width'],
                                               height: label_data['parcel']['height'],
                                               weight: label_data['parcel']['weight'],
                                               distance_unit: label_data['parcel']['distance_unit'],
                                               mass_unit: label_data['parcel']['mass_unit'])

          tracking.real_package = Package.new(carrier.get_data_package(label_data['tracking_number']))

          tracking.save
        end

      end
    end

    respond_to do |format|
        format.html { redirect_to trackings_path, notice: I18n.t('messages.created') }
    end

  end

end
