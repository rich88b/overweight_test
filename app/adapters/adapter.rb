module Adapter
  class FedexAdapter

    def initialize()
      @fedex = Fedex::Shipment.new(:key => FEDEX_ACCOUNT[:key],
                                  :password => FEDEX_ACCOUNT[:password],
                                  :account_number => FEDEX_ACCOUNT[:account_number],
                                  :meter => FEDEX_ACCOUNT[:meter],
                                  :mode => FEDEX_ACCOUNT[:mode])
    end

    def get_data_package(tracking_number)
      begin
        results = @fedex.track(:tracking_number => tracking_number)
        tracking_info = results.first

        {
          length: tracking_info.details[:package_dimensions][:length],
          width: tracking_info.details[:package_dimensions][:width],
          height: tracking_info.details[:package_dimensions][:height],
          weight: tracking_info.details[:package_weight][:value],
          distance_unit: tracking_info.details[:package_dimensions][:units],
          mass_unit: tracking_info.details[:package_weight][:units]
        }
      rescue Exception => ex # optionally: `rescue Exception => ex`
        msj =  ex.message
        puts msj
        {}
      end
    end

  end

end
