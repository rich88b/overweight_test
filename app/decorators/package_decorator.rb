class PackageDecorator < BaseDecorator
  def weight_label
    self.get_total_weight.ceil
  end
end
