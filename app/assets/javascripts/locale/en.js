var locale = {
    locale_cod: 'es-Es',
    datatable:{
        labels:{
            processing: "Processing",
            lengthMenu: "_MENU_",
            zeroRecords: "Not records",
            emptyTable: "Data empty",
            info: "Show _START_ to _END_ of _TOTAL_ records",
            infoEmpty: "Show 0 to 0 of _TOTAL_ 0 records",
            infoFiltered: "(filtered of total MAX records)",
            infoPostFix: "",
            search: "",
            url: "",
            infoThousands: ",",
            loadingRecords: "Loading...",
            notConnection: "Problems in the request",
            paginate: {
                previous: "<",
                next: ">",
                first: "...",
                last: "..."
            },
            dataTables_filter:{
                search: "Search"
            }
        },
    },

};
