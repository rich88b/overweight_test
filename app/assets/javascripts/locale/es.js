var locale = {
    locale_cod: 'es-Es',
    datatable:{
        labels:{
            processing: "Procesando",
            lengthMenu: "_MENU_",
            zeroRecords: "No se encontraron resultados",
            emptyTable: "Ningún dato disponible en esta tabla",
            info: "Mostrando del _START_ al _END_ de _TOTAL_ registros",
            infoEmpty: "Mostrando registros del 0 al 0 de un total de 0 registros",
            infoFiltered: "(filtrado de un total de MAX registros)",
            infoPostFix: "",
            search: "",
            url: "",
            infoThousands: ",",
            loadingRecords: "Cargando...",
            notConnection: "Problemas al solicitar la información",
            paginate: {
                previous: "<",
                next: ">",
                first: "...",
                last: "..."
            },
            buttons:{
                copy: "Copiar",
                excel: "Excel",
                pdf: "Pdf",
                print: "Imprimir"
            },
            dataTables_filter:{
                search: "Buscar"
            }
        },
    },

};
