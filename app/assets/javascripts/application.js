// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .

//= require bootstrap/js/bootstrap
//= require datatables/datatables

//= require dataTables/extensions/dataTables.buttons.min
//= require dataTables/extensions/buttons.html5.min
//= require dataTables/extensions/buttons.print.min
//= require dataTables/extensions/jszip.min
//= require dataTables/extensions/pdfmake.min
//= require dataTables/extensions/vfs_fonts
//= require dataTables/extensions/buttons.flash
//= require dataTables/extensions/buttons.colVis.min
//= require dataTables/extensions/dataTables.responsive.min

//-- DataTable --
$.extend( true, $.fn.dataTable.defaults, {
    dom: 'Bfrtip',
    bShowAll: true,
    processing: true,
    serverSide: true,
    pageLength: 10,
    autoWidth: true,
    searching: true,
    ordering: true,
    deferRender: true,
    responsive: true,
    language: {
        "processing": locale.datatable.labels.processing,
        "lengthMenu": "_MENU_",
        "zeroRecords": locale.datatable.labels.zeroRecords,
        "emptyTable": locale.datatable.labels.emptyTable,
        "info": locale.datatable.labels.info,
        "infoEmpty": locale.datatable.labels.infoEmpty,
        "infoFiltered": locale.datatable.labels.infoFiltered,
        "infoPostFix": locale.datatable.labels.infoPostFix,
        "search": locale.datatable.labels.search,
        "url": locale.datatable.labels.url,
        "infoThousands": locale.datatable.labels.infoThousands,
        "loadingRecords": locale.datatable.labels.loadingRecords,
        "paginate": {
            "previous": locale.datatable.labels.paginate.previous,
            "next": locale.datatable.labels.paginate.next,
            "first": locale.datatable.labels.paginate.first,
            "last": locale.datatable.labels.paginate.last
        }
    },
    paginationType: 'full_numbers',
    buttons: [
        'pdf', 'excel'
    ]
});
