$(document).ready(function(){
    var table = $("#tracking-datatable");
    table.dataTable({
        ajax: {
            url: table.data("source")
        },
        columns: [
            {data: 'tracking_number', name: 'trackings.tracking_number'},
            {data: 'carrier', name: 'trackings.carrier'},
            {data: 'weight', name: 'packages.weight', orderable: false, searchable: false,},
            {data: 'real_weight', name: 'packages.weight', orderable: false, searchable: false},
            {data: 'real_total_weight', name: 'packages.weight', orderable: false, searchable: false},
            {data: 'overweight_lbl', name: 'packages.weight', orderable: false, searchable: false,},
            {data: 'is_overweight', name: 'packages.weight', orderable: false, searchable: false,},
        ],
        "createdRow": function( row, data, dataIndex){
            if( data.overweight ==  0){
                $(row).addClass('greenClass');
            }
        }

    });

});
